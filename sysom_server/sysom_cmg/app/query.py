# -*- coding: utf-8 -*- #
"""
Time                2023/09/19 15:41
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                schemas.py
Description:
"""
from typing import Optional
from app import models
from sysom_utils import BaseQueryParams


# class PersonQueryParams(BaseQueryParams):
    
#     # 1. 指定要查询的模型
#     __modelclass__ = models.Person
    
#     # 2. 定义排序字段
#     sort: str = "-created_at"

#     # 3. 定义支持用于过滤的参数
#     name: Optional[str] = None
#     age: Optional[str] = None
    
#     # 4. 指定哪些字段是枚举类型，并且指明对应的枚举类
#     __enum_fields__ = {
#     }