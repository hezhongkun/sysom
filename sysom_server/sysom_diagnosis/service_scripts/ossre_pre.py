"""
Time                2023/012/19 17:32
Author:             chenshiyan
Email               chenshiyan@linux.alibaba.com
File                ossre_pre.py
Description:
"""
from .base import DiagnosisJob, DiagnosisPreProcessor, DiagnosisTask


class PreProcessor(DiagnosisPreProcessor):
    """ossre diagnosis
    """

    def get_diagnosis_cmds(self, params: dict) -> DiagnosisTask:
        instance = params.get("instance", "")
        command = "sysak ossre_client -s > /dev/null " + " && " + "cat /var/log/sysak/ossre.log"
        print (command)
        return DiagnosisTask(
            jobs=[
                DiagnosisJob(instance=instance, cmd=command)
            ],
            in_order=False,
        )
