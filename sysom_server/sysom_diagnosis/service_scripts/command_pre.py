"""
Time                2023/06/19 17:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                command.py
Description:
"""
from .base import DiagnosisJob, DiagnosisPreProcessor, DiagnosisTask


class PreProcessor(DiagnosisPreProcessor):
    """Command diagnosis

    Just invoke command in target instance and get stdout result

    Args:
        DiagnosisPreProcessor (_type_): _description_
    """

    def get_diagnosis_cmds(self, params: dict) -> DiagnosisTask:
        command = params.get("command", "")
        instance = params.get("instance", "")
        return DiagnosisTask(
            jobs=[
                DiagnosisJob(instance=instance, cmd=command)
            ],
            in_order=False,
        )
