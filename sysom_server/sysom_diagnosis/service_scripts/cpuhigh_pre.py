"""
Time                2023/07/27 15:22
Author:             suiya.lhl
Email               liuhailong@linux.alibaba.com
File                cpuhigh.py
Description:
"""
from .base import DiagnosisJob, DiagnosisPreProcessor, DiagnosisTask


class PreProcessor(DiagnosisPreProcessor):
	"""Command diagnosis

	Just invoke command in target instance and get stdout result

	Args:
		DiagnosisPreProcessor (_type_): _description_
	"""

	def get_diagnosis_cmds(self, params: dict) -> DiagnosisTask:
		instance = ""
		moment = params.get("moment", "")
		tmpstr = params.get("instance", "")
		if tmpstr.find(":") !=1:
			instance = tmpstr.split(":")[0]
		else:
			instance = tmpstr
		dump_log_cmd = "cat /var/log/sysak/cpuhigh/cpuhigh.json;"
		command = "sysak -g cpuhigh "+"\""+str(moment)+"\""+" > /dev/null" + " && "+dump_log_cmd 
		return DiagnosisTask(
			jobs=[DiagnosisJob(instance=instance, cmd=command)],
			in_order=False,
		)
