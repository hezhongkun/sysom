from typing import List
from .base import DiagnosisJobResult, DiagnosisPostProcessor, PostProcessResult
import json


class PostProcessor(DiagnosisPostProcessor):
    def parse_diagnosis_result(
        self, results: List[DiagnosisJobResult]
    ) -> PostProcessResult:
        preprocess_result = json.loads(results[0].stdout)
        postprocess_result = PostProcessResult(
            code=preprocess_result["code"],
            err_msg=preprocess_result["err_msg"],
            result=preprocess_result["result"],
        )

        return postprocess_result
