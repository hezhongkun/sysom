# -*- coding: utf-8 -*- #
"""
Time                2023/11/29 10:08
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                settings.py
Description:
"""
import os

env = os.environ.get("env", "product")


if env == "develop":
    from .develop import *
elif env == "testing":
    from .testing import *
elif env == "product":
    from .product import *

from .collector_settings import *
from .clusterhealth_settings import *

# Prometheus to collect metrics
PROMETHEUS_CONFIG = YAML_CONFIG.get_server_config().db.prometheus
# No Cluster Label in metric, assume all metric is in one cluster
NO_CLUSTER_LABEL = True