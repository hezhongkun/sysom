from cec_base.event import Event
from cec_base.consumer import Consumer
from cec_base.producer import Producer
from cec_base.cec_client import MultiConsumer, CecAsyncConsumeTask
from clogger import logger
from lib.common_type import Level
from app.health_calculator.health_metric import HealthMetric
from conf.settings import *
from sysom_utils import CecTarget, SysomFramework


class HealthMetricListener(MultiConsumer):
    def __init__(self) -> None:
        super().__init__(
            YAML_CONFIG.get_cec_url(CecTarget.PRODUCER),
            custom_callback=self.on_receive_event,
        )
        self.append_group_consume_task(
            CEC_TOPIC_SYSOM_HEALTH_METRIC,
            "sysom_cluster_health",
            Consumer.generate_consumer_id(),
            ensure_topic_exist=True,
        )
        
        self.gcache_cluster_metrics = SysomFramework.gcache(CLUSTER_HEALTH_METRIC_GCACHE)
        self.gcache_node_metrics = SysomFramework.gcache(NODE_HEALTH_METRIC_GCACHE)
        self.gcache_pod_metrics = SysomFramework.gcache(POD_HEALTH_METRIC_GCACHE)
        
    def _delivery(self, topic: str, value: dict):
        self._producer.produce(topic, value)
        self._producer.flush()
        
    def _deal_health_metric(self, health_metric: HealthMetric):
        health_metric = health_metric.dict()
        layer = health_metric["layer"]
        
        if layer not in [Level.Cluster.value, Level.Node.value, Level.Pod.value]:
            raise Exception(f"Invalid layer: {layer} of metric: {health_metric}")
        
        try:
            if layer == Level.Cluster.value:
                cluster = health_metric[cluster]
                self.gcache_cluster_metrics.push_list(cluster, health_metric)
            elif layer == Level.Node.value:
                # use cluster and node as key in case of same node name in different cluster
                key = f"{health_metric['cluster']}:{health_metric['node']}"
                self.gcache_node_metrics.push_list(key, health_metric)
            elif layer == Level.Pod.value:
                key = f"{health_metric['cluster']}:{health_metric['pod']}:{health_metric['namespace']}"
                self.gcache_pod_metrics.push_list(key, health_metric)
        except Exception as e:
            raise Exception(
                f"Failed to deal with health metric: {health_metric}, error: {e}"
            )

    def on_receive_event(self, event: Event, task: CecAsyncConsumeTask):
        """
        处理每个单独的任务
        """
        event_value = event.value
        try:
            assert isinstance(event_value, dict)
            if task.topic_name == CEC_TOPIC_SYSOM_HEALTH_METRIC:
                health_metric = HealthMetric(**event_value)
                self._deal_health_metric(health_metric)
            else:
                logger.warning(
                    f"Received not expect topic data, topic = {task.topic_name}"
                )
        except Exception as e:
            logger.exception(e)
        finally:
            # 执行消息确认
            task.ack(event)