import importlib
from conf.settings import *
from app.collector.metric_exception import MetricSettingsException
from app.collector.metric_type.capacity import CapacityMetric
from app.collector.metric_type.load import LoadMetric
from app.collector.metric_type.latency import LatencyMetric
from app.collector.metric_type.error import ErrorMetric
from lib.common_type import Level
from metric_reader import dispatch_metric_reader

CUSTOM_METRIC_DIR = "app.collector.custom_metric"

METRIC_TYPE = {
    "CapacityMetric": CapacityMetric,
    "LoadMetric": LoadMetric,
    "LatencyMetric": LatencyMetric,
    "ErrorMetric": ErrorMetric
}

class MetricManager():
    def __init__(self):
        self.registed_metric = {}
        self.metric_reader = dispatch_metric_reader(
            "prometheus://" + PROMETHEUS_CONFIG.host + ":" + str(PROMETHEUS_CONFIG.port))

    def _metric_register(self, all_metrics, level):
        self.registed_metric[level] = []
        
        for metric in all_metrics:
            try:
                if "filename" in metric["Collect"] \
                        and metric["Collect"]["filename"] != "":
                    # load non_standard metric from file
                    filename = metric["Collect"]["filename"]
                    module_name = f'{CUSTOM_METRIC_DIR}.{filename}'
                    class_name = filename.title().replace("_", "")
                    try:
                        metric_module = importlib.import_module(module_name)
                        if hasattr(metric_module, class_name):
                            metric_class = getattr(metric_module, class_name)
                            metric_instance = metric_class(self.metric_reader,
                                                            metric, level)
                    except ModuleNotFoundError as exc:
                        raise MetricSettingsException(
                            f"{module_name} not exist!"
                        ) from exc
                    except MetricSettingsException as exc:
                        raise exc

                else:
                    metric_class = METRIC_TYPE.get(metric["Type"])
                    if metric_class is not None:
                        metric_instance = metric_class(
                            self.metric_reader, metric, level)

            except Exception as exc:
                raise MetricSettingsException(
                    f"Collector setting error of metric {metric}: {exc}"
                ) from exc

            self.registed_metric[level].append(
                metric_instance)

    def metric_register(self):
        """
        Register all metrics to metric manager from settings
        """
        try:
            self._metric_register(CLUSTER_METRICS, Level.Cluster)
            self._metric_register(POD_METRICS, Level.Pod)
            self._metric_register(NODE_METRICS, Level.Node)
        except MetricSettingsException as exc:
            raise exc
