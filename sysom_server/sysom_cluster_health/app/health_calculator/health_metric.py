from pydantic import BaseModel
from collections import OrderedDict
from typing import Optional, Dict, List

class HealthMetric(BaseModel):
    metric_id:      str
    process_time:   float
    event_time:     float
    score:          float
    value:          float
    layer:          str
    cluster:        str
    node:           Optional[str]
    pod:            Optional[str]
    namespace:      Optional[str]
    
class HealthMetricsMap:
    def __init__(self, capacity: int = 5):
        self.capacity = capacity
        self.metrics_map = OrderedDict()
    
    def add_metric(self, metric: HealthMetric):
        if metric.metric_id in self.metrics_map:
            self.metrics_map[metric.metric_id] = metric
        else:
            if len(self.metrics_map) >= self.capacity:
                self.metrics_map.popitem(last=False)
            self.metrics_map[metric.metric_id] = metric
        self.metrics_map = OrderedDict(
            sorted(self.metrics_map.items(), key=lambda item: item[1].score, reverse=True)
        )