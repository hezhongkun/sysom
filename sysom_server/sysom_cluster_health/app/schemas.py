# -*- coding: utf-8 -*- #
"""
Time                2023/11/29 10:08
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                schemas.py
Description:
"""
from pydantic import BaseModel
from typing import Optional

###########################################################################
# Define schemas here
###########################################################################

# @reference https://fastapi.tiangolo.com/zh/tutorial/response-model/
# class Person(BaseModel):
#     id: int
#     name: str
#     age: int
#     created_at: datetime
    
#     class Config:
#         orm_mode = True


class AbnormalMetricsBase(BaseModel):
    metric_id: str
    metric_type: str
    cluster: str
    instance: Optional[str]
    namespace: Optional[str]
    pod: Optional[str]
    score: float
    value: float
    timestamp: float