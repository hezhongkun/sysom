from dataclasses import dataclass
from typing import Optional, Dict, List
from lib.common_type import Level

@dataclass
class DiagnoseInfo:
    alarm_id: str
    service_name: str
    type: str
    level: Level
    metric_id: str
    instance: str
    pod: Optional[str] = None
    container: Optional[str] = None
    time: Optional[str] = None
    diagnose_type: Optional[str] = None