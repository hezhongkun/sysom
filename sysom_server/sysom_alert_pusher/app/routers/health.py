# -*- coding: utf-8 -*- #
"""
Time                2023/11/23 19:11
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                health.py
Description:
"""
from clogger import logger
from typing import Dict
from fastapi import APIRouter, Body


router = APIRouter()


@router.get("/check")
async def helth_check():
    return {
        "code": 0,
        "err_msg": "",
        "data": ""
    }
    
@router.post("/dummy_echo")
async def dummy_echo(item: Dict = Body(...)):
    return {
        "code": 0,
        "err_msg": "",
        "data": item
    }
