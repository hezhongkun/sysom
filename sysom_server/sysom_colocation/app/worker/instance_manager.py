from metric_reader.metric_reader import MetricReader
from sysom_utils import SysomFramework
from time import time
import conf.settings as settings
from lib.table import *
from lib.algo import PeriodAlgo
from clogger import logger
from lib.utils import generate_unique_key, ts_2_hour
import json


class MetricAdpater:
    def __init__(
        self,
        predict_table: PredictTable,
        allocation_table: AllocationTable,
        algo: PeriodAlgo,
        rt_gcache: SysomFramework.gcache,
        future_gcache: SysomFramework.gcache,
    ) -> None:
        self._predict_table = predict_table
        self._allocation_table = allocation_table
        self._algo = algo
        self._rt_gcache = rt_gcache
        self._future_gcache = future_gcache

    def _generate_key(self, *arg, **kwargs) -> str:
        return generate_unique_key(
            self._predict_table._cluster,
            self._predict_table._instance,
            self._predict_table._resource,
            self._predict_table._tag,
            *arg,
            **kwargs,
        )

    def _generate_rt_value(self, category, value) -> str:
        values = {
            "cluster": self._predict_table._cluster,
            "instance": self._predict_table._instance,
            "resource": self._predict_table._resource,
            "tag": self._predict_table._tag,
            "category": category,
            "value": value,
        }
        return json.dumps(values)

    def _generate_future_value(self, category, value, future) -> str:
        values = {
            "cluster": self._predict_table._cluster,
            "instance": self._predict_table._instance,
            "resource": self._predict_table._resource,
            "tag": self._predict_table._tag,
            "category": category,
            "future": future,
            "value": value,
        }
        return json.dumps(values)

    def future_call(self) -> None:
        if self._future_gcache is None:
            return

        logger.debug("future_call is running...")
        allocation = None
        # calculate the future watermark(slack and predict)
        future_watermark = self._algo.predict_future()
        if self._allocation_table is not None:
            allocation = self._allocation_table.query_allocation()
            if allocation is None:
                logger.error(
                    f"Query resource allocation failed. table_name = {self._allocation_table._table_name}"
                )

        current = ts_2_hour(time())
        for offset in range(24):
            index = (current + offset) % 24
            watermark = future_watermark[index]
            logger.debug(
                f"store the future watermark offset={offset} watermark={watermark}..."
            )
            self._future_gcache.store(
                self._generate_key(settings.CATEGORY_PERDICT, str(offset)),
                self._generate_future_value(
                    settings.CATEGORY_PERDICT, watermark, offset
                ),
            )
            if allocation is None:
                continue
            slack = max(0, allocation - watermark) * settings.SLACK_FACTOR
            self._future_gcache.store(
                self._generate_key(settings.CATEGORY_SLACK, str(offset)),
                self._generate_future_value(settings.CATEGORY_SLACK, slack, offset),
            )

    def rt_call(self, start_time: float, end_time: float) -> None:
        df = self._predict_table.range_query(start_time, end_time)
        for _, row in df.iterrows():
            self._algo.learn(row["ts"], row["metric"])
        util = self._algo.predict(time())
        self._rt_gcache.store(
            self._generate_key(settings.CATEGORY_PERDICT),
            self._generate_rt_value(settings.CATEGORY_PERDICT, util),
        )

        # calculate the slack resource. only for LS.
        if self._allocation_table is not None:
            allocation = self._allocation_table.query_allocation()
            if allocation is None:
                logger.error(
                    f"Query resource allocation failed. table_name = {self._allocation_table._table_name}"
                )
                return
            slack = max(allocation - util, 0) * settings.SLACK_FACTOR

            self._rt_gcache.store(
                self._generate_key(settings.CATEGORY_SLACK),
                self._generate_rt_value(settings.CATEGORY_SLACK, slack),
            )


class InstanceManager:
    def __init__(
        self,
        cluster: str,
        instance: str,
        reader: MetricReader,
        rt_gcache: SysomFramework.gcache,
        future_gcache: SysomFramework.gcache,
    ) -> None:
        self._cluster = cluster
        self._instance = instance
        self._metric_reader = reader
        self._rt_gcache = rt_gcache
        self._future_gcache = future_gcache
        self._last_rt_call = time() - settings.DEFAULT_HISTORY_SPAN_SECS
        self._last_future_call = None
        self._cpu_max = ProcCpusTable(
            self._metric_reader,
            self._cluster,
            self._instance,
            settings.RESOURCE_CPU,
            settings.TAG_ALL,
        ).query_max()
        self._mem_max = ProcMeminfoTable(
            self._metric_reader,
            self._cluster,
            self._instance,
            settings.RESOURCE_MEMORY,
            settings.TAG_ALL,
            -1,
        ).query_max()

        if self._cpu_max is None or self._mem_max is None:
            logger.error(f"InstanceManager init failed. get max failed.")
        self._adapters = []
        self.init_adapters()

    def default_algo(self) -> PeriodAlgo:
        return PeriodAlgo(
            settings.ALGO_WINDOW_MINUTES,
            settings.ALGO_SLOT_INTERVAl,
            settings.ALGO_HALF_LIFE,
        )

    def init_adapters(self) -> None:
        self.create_cpu_metric_adapters()
        self.create_mem_metric_adapters()

    def create_mem_metric_adapters(self) -> None:
        # Node Memory Predict
        self._adapters.append(
            MetricAdpater(
                ProcMeminfoTable(
                    self._metric_reader,
                    self._cluster,
                    self._instance,
                    settings.RESOURCE_MEMORY,
                    settings.TAG_ALL,
                    self._mem_max,
                ),
                None,
                self.default_algo(),
                self._rt_gcache,
                self._future_gcache,
            )
        )

        # Ls Memory Predict & Ls Memory Slack
        self._adapters.append(
            MetricAdpater(
                ContainerMemutilTable(
                    self._metric_reader,
                    self._cluster,
                    self._instance,
                    settings.RESOURCE_MEMORY,
                    settings.TAG_LS,
                    self._mem_max,
                    settings.TAG_LS,
                ),
                ContainerMemutilTable(
                    self._metric_reader,
                    self._cluster,
                    self._instance,
                    settings.RESOURCE_MEMORY,
                    settings.TAG_LS,
                    self._mem_max,
                    settings.TAG_LS,
                ),
                self.default_algo(),
                self._rt_gcache,
                self._future_gcache,
            )
        )

        # BE Memory Predict
        self._adapters.append(
            MetricAdpater(
                ContainerMemutilTable(
                    self._metric_reader,
                    self._cluster,
                    self._instance,
                    settings.RESOURCE_MEMORY,
                    settings.TAG_BE,
                    self._mem_max,
                    settings.TAG_BE,
                ),
                None,
                self.default_algo(),
                self._rt_gcache,
                None,
            )
        )

        # NOR Memory Predict
        self._adapters.append(
            MetricAdpater(
                ContainerMemutilTable(
                    self._metric_reader,
                    self._cluster,
                    self._instance,
                    settings.RESOURCE_MEMORY,
                    settings.TAG_NOR,
                    self._mem_max,
                    settings.TAG_NOR,
                ),
                None,
                self.default_algo(),
                self._rt_gcache,
                None,
            )
        )

    def create_cpu_metric_adapters(self) -> None:
        # Node CPU
        self._adapters.append(
            MetricAdpater(
                ProcCpuTotalTable(
                    self._metric_reader,
                    self._cluster,
                    self._instance,
                    settings.RESOURCE_CPU,
                    settings.TAG_ALL,
                    100,
                ),
                None,
                self.default_algo(),
                self._rt_gcache,
                self._future_gcache,
            )
        )
        # LS CPU
        self._adapters.append(
            MetricAdpater(
                ContainerCpuacctStatTable(
                    self._metric_reader,
                    self._cluster,
                    self._instance,
                    settings.TAG_LS,
                    settings.RESOURCE_CPU,
                    settings.TAG_LS,
                    self._cpu_max,
                ),
                ContainerCfsQuotaTable(
                    self._metric_reader,
                    self._cluster,
                    self._instance,
                    settings.RESOURCE_CPU,
                    settings.TAG_LS,
                    self._cpu_max,
                ),
                self.default_algo(),
                self._rt_gcache,
                self._future_gcache,
            )
        )

        # BE CPU
        self._adapters.append(
            MetricAdpater(
                ContainerCpuacctStatTable(
                    self._metric_reader,
                    self._cluster,
                    self._instance,
                    settings.TAG_BE,
                    settings.RESOURCE_CPU,
                    settings.TAG_BE,
                    self._cpu_max,
                ),
                None,
                self.default_algo(),
                self._rt_gcache,
                None,
            )
        )

        # NOR CPU
        self._adapters.append(
            MetricAdpater(
                ContainerCpuacctStatTable(
                    self._metric_reader,
                    self._cluster,
                    self._instance,
                    settings.TAG_NOR,
                    settings.RESOURCE_CPU,
                    settings.TAG_NOR,
                    self._cpu_max,
                ),
                None,
                self.default_algo(),
                self._rt_gcache,
                None,
            )
        )

    def call(self) -> None:
        """ This function will notify all adapter get the data in range and feed to model.\
            than predict current watermark. \
            If it's the first evoke, it will try learn the history data.
        """
        logger.debug("InstanceManager call running...")
        now = time()
        for adapter in self._adapters:
            start = self._last_rt_call
            end = start + settings.SEC_PER_DAY
            while True:
                adapter.rt_call(start_time=start, end_time=min(end, now))
                if end > now:
                    break
                start = end
                end = start + settings.SEC_PER_DAY
        self._last_rt_call = now

        current = ts_2_hour(time())
        if self._last_future_call is None or self._last_future_call != current:
            for adapter in self._adapters:
                adapter.future_call()
            self._last_future_call = current
