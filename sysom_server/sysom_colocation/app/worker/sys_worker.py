from time import time, sleep
import conf.settings as settings
from multiprocessing import Process
from schedule import Scheduler
from os import getpid
from conf.common import PROMETHEUS_DATABASE_URL
from sysom_utils import SysomFramework
from clogger import logger
from metric_reader import dispatch_metric_reader
from lib.table import ContainerCpuacctStatTable
from lib.pod_state import pod_mgr
from lib.utils import (
    collect_all_clusters,
    collect_instances_of_cluster,
    generate_unique_key,
)
import uuid


class InstanceSysChecker:
    def __init__(self, cluster: str, instance: str, metric_reader) -> None:
        self._cluster = cluster
        self._instance = instance
        self._table = ContainerCpuacctStatTable(
            metric_reader, self._cluster, self._instance, None, None, None, None
        )
        self._last_check = time() - settings.DEFAULT_FIRST_CHECK_INTERVAL

    def _check_all_pod_sys(self):
        now = time()
        pod_list = self._table.query_cpu_time(self._last_check, now)
        for pod in pod_list:
            for user, sys in zip(pod["user"], pod["system"]):
                alarm = pod_mgr.sys_alarm(
                    self._cluster,
                    self._instance,
                    pod["ns"],
                    pod["pod"],
                    pod["con"],
                    float(sys[1]),
                    float(user[1]),
                )
                if alarm is not None:
                    # quota = alarm
                    logger.info(
                        f"[SysWorker] check sys alarm: ts={time()} pod={pod['ns']}-{pod['pod']}-container:{pod['con']} sys={float(sys[1])} user={float(user[1])}."
                    )
                    SysomFramework.alarm(
                        {
                            "alert_id": str(uuid.uuid4()),
                            "instance": self._instance,
                            "alert_item": "System Time Anormal",
                            "alert_category": "MONITOR",
                            "alert_source_type": "SysOM",
                            "alert_time": int(round(time() * 1000)),
                            "status": "FIRING",
                            "alert_level": "WARNING",
                            "labels": {},
                            "annotations": {
                                "summary": f"{pod['ns']}-{pod['pod']}-{pod['con']} System时间占比偏高, 可能存在oncpu干扰"
                            },
                        }
                    )

        self._last_check = now

    def call(self):
        self._check_all_pod_sys()


class SysWorker(Process):
    def __init__(self, interval_sec: int = 15) -> None:
        super().__init__(daemon=True)
        self.interval_sec = interval_sec
        self.scheduler: Scheduler = Scheduler()
        self.current_pid = getpid()
        self.metric_reader = dispatch_metric_reader(PROMETHEUS_DATABASE_URL)
        self.instances = {}

    def _check_instance(self) -> None:
        cluster_list = collect_all_clusters(self.metric_reader)

        # no cluster label, we assume just one, and names it "dafault"
        if len(cluster_list) == 0 or settings.NO_CLUSTER_LABEL:
            cluster_list.append("default")

        for cluster in cluster_list:
            instance_list = collect_instances_of_cluster(
                cluster, self.metric_reader, 60
            )
            for instance in instance_list:
                unique_key = generate_unique_key(cluster, instance)
                if unique_key in self.instances.keys():
                    continue
                logger.info(
                    f"create new InstanceSysChecker cluster is {cluster} instance is {instance}"
                )
                self.instances[unique_key] = InstanceSysChecker(
                    cluster, instance, self.metric_reader
                )

    def _update(self) -> None:
        logger.debug(f"Sys Worker alive...")
        self._check_instance()
        for ins in self.instances.values():
            ins.call()

    def run(self) -> None:
        logger.info(f"Sys Worker running on pid {self.current_pid}")

        self._update()
        self.scheduler.every(self.interval_sec).seconds.do(self._update)

        while True:
            if self.is_alive():
                try:
                    self.scheduler.run_pending()
                except Exception as e:
                    logger.error(f"Sys Worker error: {e}")
                finally:
                    sleep(max(1, int(self.interval_sec / 2)))
            else:
                break
