from datetime import datetime
from time import time, sleep
import conf.settings as settings
from multiprocessing import Process
from schedule import Scheduler
from os import getpid
from conf.common import PROMETHEUS_DATABASE_URL
from sysom_utils import SysomFramework
from clogger import logger
from metric_reader import dispatch_metric_reader
from lib.table import ContaierCfsStatistics
from lib.pod_state import pod_mgr
from lib.utils import (
    collect_all_clusters,
    collect_instances_of_cluster,
    generate_unique_key,
)
import uuid


class InstanceServeUtilChecker:
    def __init__(self, cluster: str, instance: str, metric_reader) -> None:
        self._cluster = cluster
        self._instance = instance
        self._table = ContaierCfsStatistics(metric_reader, cluster, instance)
        self._last_check = time() - settings.DEFAULT_FIRST_CHECK_INTERVAL

    def _check_all_pod_serve_util(self):
        now = time()
        container_list = self._table.query_serve_util_rate(self._last_check, now)
        for item in container_list:
            for val in item["serveutil"]:
                ts = float(val[0])
                util = float(val[1])
                if util == 0:
                    # util is zero means that current system not support the metric
                    continue
                alarm = pod_mgr.cfs_alarm(
                    self._cluster,
                    self._instance,
                    item["ns"],
                    item["pod"],
                    item["con"],
                    util,
                )
                if alarm is not None:
                    logger.info(
                        f"[ServeUtil] check Serveutil alarm: ts={ts} pod={item['ns']}-{item['pod']}-container:{item['con']} serveutil={util}."
                    )
                    alert_id = str(uuid.uuid4())
                    SysomFramework.alarm(
                        {
                            "alert_id": alert_id,
                            "instance": self._instance,
                            "alert_item": "Service Utilization Anormal",
                            "alert_category": "MONITOR",
                            "alert_source_type": "SysOM",
                            "alert_time": int(round(time() * 1000)),
                            "status": "FIRING",
                            "alert_level": "WARNING",
                            "labels": {},
                            "annotations": {
                                "summary": f"{item['ns']}-{item['pod']}-{item['con']} CFS满足率较低, 可能存在on queue干扰",
                                "SYSOM_ALARM:OPT:sysom_diagnose:colocation_serveutil_diagnose": {
                                    "label": "根因分析",
                                    "type": "LINK",
                                    "url": f"/diagnose/colocation/serveutil?instance={self._instance}&moment={datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')}",
                                },
                            },
                        }
                    )

                    logger.debug(f"Serveutl alarm add action: alert_id={alert_id}")

        self._last_check = now

    def call(self):
        self._check_all_pod_serve_util()


class ServeUtilWorker(Process):
    def __init__(self, interval_sec: int = 15) -> None:
        super().__init__(daemon=True)
        self.interval_sec = interval_sec
        self.scheduler: Scheduler = Scheduler()
        self.current_pid = getpid()
        self.metric_reader = dispatch_metric_reader(PROMETHEUS_DATABASE_URL)
        self.instances = {}

    def _check_instance(self) -> None:
        cluster_list = collect_all_clusters(self.metric_reader)

        # no cluster label, we assume just one, and names it "dafault"
        if len(cluster_list) == 0 or settings.NO_CLUSTER_LABEL:
            cluster_list.append("default")

        for cluster in cluster_list:
            instance_list = collect_instances_of_cluster(
                cluster, self.metric_reader, 60
            )
            for instance in instance_list:
                unique_key = generate_unique_key(cluster, instance)
                if unique_key in self.instances.keys():
                    continue

                self.instances[unique_key] = InstanceServeUtilChecker(
                    cluster, instance, self.metric_reader
                )

    def _update(self) -> None:
        logger.debug(f"ServeUtil Worker alive...")
        self._check_instance()
        for ins in self.instances.values():
            ins.call()

    def run(self) -> None:
        logger.info(f"ServeUtil Worker running on pid {self.current_pid}")
        self._update()
        self.scheduler.every(self.interval_sec).seconds.do(self._update)

        while True:
            if self.is_alive():
                try:
                    self.scheduler.run_pending()
                except Exception as e:
                    logger.error(f"ServeUtil Worker error: {e}")
                finally:
                    sleep(max(1, int(self.interval_sec / 2)))
            else:
                break
