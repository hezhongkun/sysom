from abc import abstractmethod
import threading
from time import time
from typing import List, Tuple
from lib.table import (
    ContaierCfsStatistics,
    ContainerCfsQuotaTable,
    ContainerCpuacctStatTable,
    ContainerMemutilTable,
    ContainerPmuEvents,
)
from metric_reader.metric_reader import dispatch_metric_reader, MetricReader
from conf.common import PROMETHEUS_DATABASE_URL
from lib.utils import generate_unique_key, get_today_zero_ts
import conf.settings as settings
from clogger import logger


def get_max_resource_interval(last_update_time: float):
    interval = None
    if last_update_time is None:
        interval = "1d"
    elif time() - last_update_time >= settings.SEC_PER_HOUR:
        interval = "1h"
    return interval


class PodMetric:
    def __init__(
        self, cluster: str, instance: str, ns: str, pod: str, container: str
    ) -> None:
        self._cluster = cluster
        self._instance = instance
        self._ns = ns
        self._pod = pod
        self._con = container
        self._value = 0.0
        self._last_update_time = None

    @abstractmethod
    def value(self) -> float:
        if self._last_update_time is not None:
            return self._value
        else:
            return None


class PodMaxCPU(PodMetric):
    def __init__(
        self,
        cluster: str,
        instance: str,
        ns: str,
        pod: str,
        container: str,
        metric_reader: MetricReader,
    ) -> None:
        PodMetric.__init__(self, cluster, instance, ns, pod, container)
        self._cpu_table = ContainerCpuacctStatTable(
            metric_reader,
            cluster,
            instance,
            settings.TAG_OTHER,
            settings.RESOURCE_CPU,
            settings.TAG_OTHER,
            0,
        )

    def _update_cpu_max(self) -> None:
        # get the query time interval
        interval = get_max_resource_interval(self._last_update_time)
        if interval is None:
            return

        history_max_cpu = self._cpu_table.query_history_max(
            self._ns, self._pod, self._con, self._last_update_time, interval
        )
        if history_max_cpu is None:
            return

        self._value = max(self._value, history_max_cpu)
        self._last_update_time = time()

    def value(self) -> float:
        self._update_cpu_max()
        return super().value()


class PodMaxMem(PodMetric):
    def __init__(
        self,
        cluster: str,
        instance: str,
        ns: str,
        pod: str,
        container: str,
        metric_reader: MetricReader,
    ) -> None:
        PodMetric.__init__(self, cluster, instance, ns, pod, container)
        self._mem_table = ContainerMemutilTable(
            metric_reader,
            cluster,
            instance,
            settings.RESOURCE_MEMORY,
            settings.TAG_OTHER,
            0,
            settings.TAG_OTHER,
        )

    def _update_mem_max(self) -> None:
        # get the query time interval
        interval = get_max_resource_interval(self._last_update_time)
        if interval is None:
            return

        history_max_mem = self._mem_table.query_history_max(
            self._ns, self._pod, self._con, self._last_update_time, interval
        )
        if history_max_mem is None:
            return
        self._value = max(self._value, history_max_mem)
        self._last_update_time = time()

    def value(self) -> float:
        self._update_mem_max()
        return super().value()


class PodMeanCPI(PodMetric):
    def __init__(
        self,
        cluster: str,
        instance: str,
        ns: str,
        pod: str,
        container: str,
        metric_reader: MetricReader,
    ) -> None:
        PodMetric.__init__(self, cluster, instance, ns, pod, container)
        self.pmu_table = ContainerPmuEvents(metric_reader, cluster, instance)

    def _update_mean_cpi(self) -> None:
        end = get_today_zero_ts()
        if (
            self._last_update_time is None
            or (end - self._last_update_time) >= settings.SEC_PER_DAY
        ):
            mean_cpi = self.pmu_table.query_history_mean(
                self._ns, self._pod, self._con, end, "1d"
            )

            if mean_cpi is not None:
                self._value = mean_cpi
                self._last_update_time = end

    def value(self) -> float:
        self._update_mean_cpi()
        return super().value()


class PodStddevCPI(PodMetric):
    def __init__(
        self,
        cluster: str,
        instance: str,
        ns: str,
        pod: str,
        container: str,
        metric_reader: MetricReader,
    ) -> None:
        PodMetric.__init__(self, cluster, instance, ns, pod, container)
        self.pmu_table = ContainerPmuEvents(metric_reader, cluster, instance)

    def _update_stddev_cpi(self) -> None:
        end = get_today_zero_ts()
        if (
            self._last_update_time is None
            or (end - self._last_update_time) >= settings.SEC_PER_DAY
        ):
            stddev_cpi = self.pmu_table.query_history_stddev(
                self._ns, self._pod, self._con, end, "1d"
            )

            if stddev_cpi is not None:
                self._value = stddev_cpi
                self._last_update_time = end

    def value(self) -> float:
        self._update_stddev_cpi()
        return super().value()


class PodCpuQuota(PodMetric):
    def __init__(
        self,
        cluster: str,
        instance: str,
        ns: str,
        pod: str,
        container: str,
        metric_reader: MetricReader,
    ) -> None:
        PodMetric.__init__(self, cluster, instance, ns, pod, container)
        self._cpu_table = ContainerCfsQuotaTable(
            metric_reader,
            cluster,
            instance,
            settings.RESOURCE_CPU,
            settings.TAG_LS,
            None,
        )

    def _update_quota(self) -> None:
        now = time()
        if (
            self._last_update_time is None
            or (now - self._last_update_time) >= settings.SEC_PER_HOUR
        ):
            val = self._cpu_table.query_latest_quota_ratio(
                self._ns, self._pod, self._con
            )
            if val is not None:
                self._value = val
                self._last_update_time = now

    def value(self) -> float:
        self._update_quota()
        return super().value()


class PodCFS(PodMetric):
    """P99 CFS satisfication of last week, update per hour"""

    def __init__(
        self,
        cluster: str,
        instance: str,
        ns: str,
        pod: str,
        container: str,
        metric_reader: MetricReader,
    ) -> None:
        PodMetric.__init__(self, cluster, instance, ns, pod, container)
        self._cfs_table = ContaierCfsStatistics(metric_reader, cluster, instance)

    def _update_cfs(self) -> None:
        now = time()
        if (
            self._last_update_time is None
            or (now - self._last_update_time) >= settings.SEC_PER_HOUR * 1
        ):
            # query
            val = self._cfs_table.query_p99_one_week(
                self._ns, self._pod, self._con, now
            )
            if val is not None:
                self._value = val
                self._last_update_time = now

    def value(self) -> float:
        self._update_cfs()
        return super().value()


class PodInfo:
    def __init__(
        self,
        cluster: str,
        instance: str,
        ns: str,
        pod: str,
        container: str,
        metric_reader: MetricReader,
    ) -> None:
        self._cluster = cluster
        self._instance = instance
        self._ns = ns
        self._pod = pod
        self._con = container
        self._max_cpu = PodMaxCPU(cluster, instance, ns, pod, container, metric_reader)
        self._max_mem = PodMaxMem(cluster, instance, ns, pod, container, metric_reader)
        self._stddev_cpi = PodStddevCPI(
            cluster, instance, ns, pod, container, metric_reader
        )
        self._mean_cpi = PodMeanCPI(
            cluster, instance, ns, pod, container, metric_reader
        )
        self._quota = PodCpuQuota(cluster, instance, ns, pod, container, metric_reader)
        self._cfs = PodCFS(cluster, instance, ns, pod, container, metric_reader)
        self.cpi_anormal_list = []

    def query_history_max(self, resource: str) -> float:
        if resource == settings.RESOURCE_CPU:
            return self._max_cpu.value()
        elif resource == settings.RESOURCE_MEMORY:
            return self._max_mem.value()
        else:
            logger.error("Not supported resource type.")

    def remove_cpi_ts(self):
        """remove the timeout CPI, we only count the last 5 mintes"""
        now = time()
        while len(self.cpi_anormal_list) != 0:
            if now - self.cpi_anormal_list[0] > settings.DEFAULT_CPI2_PERIOD:
                self.cpi_anormal_list.pop(0)
            else:
                break

    def cpi2_alarm(self, cur_cpi: float, ts: float) -> List[float]:
        self.remove_cpi_ts()
        cpi_stddev = self._stddev_cpi.value()
        cpi_mean = self._mean_cpi.value()
        if cpi_stddev is not None and cpi_mean is not None:
            if cur_cpi > cpi_mean + 2 * cpi_stddev:
                self.cpi_anormal_list.append(ts)
                logger.info(
                    f"[cpi2_alarm] one abnormal{self._ns}-{self._pod}-{self._con} cur_cpi={cur_cpi} stddev={cpi_stddev} avg={cpi_mean}, pod={self._ns}-{self._pod}-{self._con}"
                )
        else:
            logger.error(
                f"[cpi2_alarm] {self._ns}-{self._pod}-{self._con} stddev={cpi_stddev} avg={cpi_mean}, pod={self._ns}-{self._pod}-{self._con}"
            )

        if len(self.cpi_anormal_list) >= 3:
            logger.info(
                f"[cpi2_alarm] generate alarm. {self._ns}-{self._pod}-{self._con} cur_cpi={cur_cpi} stddev={cpi_stddev} avg={cpi_mean}, pod={self._ns}-{self._pod}-{self._con}"
            )
            self.cpi_anormal_list.clear()
            return cpi_mean, cpi_stddev
        else:
            return None

    def cfs_alarm(self, cur_cfs: float) -> float:
        p99_val = self._cfs.value()
        if p99_val is None:
            logger.error(f"[cfs_alarm] {self._ns}-{self._pod}-{self._con} p99 is None.")

        return p99_val if cur_cfs < p99_val else None

    def sys_alarm(self, cur_sys: float, cur_user: float) -> float:
        quota_val = self._quota.value()

        if quota_val is None:
            logger.error(
                f"[sys_alarm] {self._ns}-{self._pod}-{self._con} quota is None."
            )
            return quota_val
        if (
            cur_sys > 0.5 * quota_val
            or ((cur_sys + cur_user) > 0.2 * quota_val and cur_sys / cur_user) > 1.5
        ):
            return quota_val
        else:
            return None


class PodStateManager:
    def __init__(self) -> None:
        self._pod_dict = {}
        self._metric_reader = dispatch_metric_reader(PROMETHEUS_DATABASE_URL)
        self._lock = threading.Lock()

    def init_pod_info(self, cluster: str, instance: str, ns: str, pod: str, con: str):
        key = generate_unique_key(cluster, instance, ns, pod, con)
        if key not in self._pod_dict.keys():
            self._pod_dict[key] = PodInfo(
                cluster, instance, ns, pod, con, self._metric_reader
            )
        return key

    def query_max(
        self, cluster: str, instance: str, ns: str, pod: str, con: str, resource: str
    ) -> float:
        with self._lock:
            key = self.init_pod_info(cluster, instance, ns, pod, con)
            return self._pod_dict[key].query_history_max(resource)

    def cpi2_alarm(
        self,
        cluster: str,
        instance: str,
        ns: str,
        pod: str,
        con: str,
        cpi: float,
        ts: float,
    ) -> Tuple[float, float]:
        with self._lock:
            key = self.init_pod_info(cluster, instance, ns, pod, con)
            return self._pod_dict[key].cpi2_alarm(cpi, ts)

    def sys_alarm(
        self,
        cluster: str,
        instance: str,
        ns: str,
        pod: str,
        con: str,
        sys: float,
        user: float,
    ) -> float:
        with self._lock:
            key = self.init_pod_info(cluster, instance, ns, pod, con)
            return self._pod_dict[key].sys_alarm(sys, user)

    def cfs_alarm(
        self,
        cluster: str,
        instance: str,
        ns: str,
        pod: str,
        con: str,
        cfs: float,
    ) -> float:
        with self._lock:
            key = self.init_pod_info(cluster, instance, ns, pod, con)
            return self._pod_dict[key].cfs_alarm(cfs)


pod_mgr = PodStateManager()
