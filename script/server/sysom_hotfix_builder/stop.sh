#!/bin/bash
SERVICE_NAME=sysom-hotfix-builder

stop_app() {
    # kill all kpatch-build process
    echo "find : `ps -eo comm,pid | grep "kpatch-build"`"
    for each_line in `ps -eo comm,pid | grep "kpatch-build"`; do
        echo $each_line
        if [[ $each_line =~ "kpatch-build" ]]; then
                echo "find kpatch-build"
        else
                kill -9 $each_line
        fi
    done

    supervisorctl stop $SERVICE_NAME
}

stop_app
