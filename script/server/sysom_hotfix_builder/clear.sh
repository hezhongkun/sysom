#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=sysom-hotfix-builder

clear_app() {
    LOCAL_NFS_HOME=${SERVER_HOME}/builder/hotfix
    sed -i '/hotfix_builder/d' /etc/exports
    
    result=$(systemctl is-active nfs-server)
    if [[ $result =~ "inactive" ]]; then
        systemctl start nfs-server
    fi
    umount -f $LOCAL_NFS_HOME
    rm -rf ${SERVER_HOME}/builder
    systemctl stop nfs-server
    rm -rf /etc/supervisord.d/${SERVICE_NAME}.ini
    systemctl stop nfs-server
    ###use supervisorctl update to stop and clear services###
    supervisorctl update
}

# Stop first
bash -x $BaseDir/stop.sh

clear_app
