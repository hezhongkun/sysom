import {  useRef, useState, useEffect, useContext, useReducer } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import { delOSType, delKernelVersion, getOSTypeList, getKernelVersionList, submitOSType, submitKernelVersion } from '../../service';
import VersionConfigList from './KernelConfig'; 

const KernelVersionConfigList = () => {
  const actionRef = useRef();
  const intl = useIntl();
  const [dataostype, setDataOsType] = useState([]);

  useEffect(()=>{
    DataOptionList();
  },[]);

  const DataOptionList = async ()=>{
      const {data} = await getOSTypeList();
      let arr = [];
      if(data?.length > 0){
          data.forEach((i)=>{
              arr.push({label: i.os_type,value: i.os_type})
          })
      }
      setDataOsType({arr:arr});
  }

  return (
    <PageContainer>
      <VersionConfigList OSTypedata={dataostype.arr} />
    </PageContainer>
  );
};

export default KernelVersionConfigList; 