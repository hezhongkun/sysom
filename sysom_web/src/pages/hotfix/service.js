import { async } from "@antv/x6/lib/registry/marker/async";
import { data } from "browserslist";
import { request } from "umi";

export async function getHotfixList(params, options) {
    const token = localStorage.getItem('token');
    return request('/api/v1/hotfix/get_hotfix_list/', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    params: params,
    ...(options || {}),
  });
}

export async function queryFormalHotfixList(params, options) {
  const token = localStorage.getItem('token');
  return request('/api/v1/hotfix/get_formal_hotfix_list/', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    params: params,
    ...(options || {}),
  });
}

export async function delHotfix(id, token, options) {
  return request('/api/v1/hotfix/delete_hotfix/', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: {
      id
    },
    ...(options || {}),
  })
}

export async function setFormal(id, token, options) {
  return request('/api/v1/hotfix/set_formal/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: {
      id 
    },
    ...(options || {}),
  })
}

export const normFile = (e) => {
  if (Array.isArray(e)) {
    return e;
  }
  return e?.fileList;
};

export const uploadProps = {
  name:'patch_file',
  action: '/api/v1/hotfix/upload_patch/',
  headers: {
    'Authorization': localStorage.getItem('token'),
  },
  accept:".patch,.diff",
  method:"post",
  onChange({ file, fileList }) {
    if (file.status !== 'uploading') {
      return fileList[0];
    }
    if (file.status === 'done'){
      return file.response.data.patch_name;
    } else if (file.status === 'error') {
      return file.response.data.patch_name;
    }
  },
  maxCount:1,
}

export const regularuploadProps = {
  name:'patch_file',
  action: '/api/v1/hotfix/upload_patch/',
  headers: {
    'Authorization': localStorage.getItem('token'),
  },
  accept:".patch,.sh,.py",
  method:"post",
  onChange({ file, fileList }) {
    if (file.status !== 'uploading') {
      return fileList[0];
    }
    if (file.status === 'done'){
      return file.response.data.patch_name;
    } else if (file.status === 'error') {
      return file.response.data.patch_name;
    }
  },
  maxCount:1,
}

export async function createHotfix(token, params, options) {
  return request('/api/v1/hotfix/create_hotfix/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: {
      'kernel_version': params.kernel_version,
      'hotfix_name': params.hotfix_name,
      'os_type': params.os_type,
      'patch_file_name': params.upload
    },
    ...(options || {}),
  })
}

export function getHotfixLog(id, options) {
  const token = localStorage.getItem('token');
  return request('/api/v1/hotfix/get_build_log/', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    params: {
      id
    },
    ...(options || {}),
  });
}

export function downloadHotfixFile(id, options){
  const token = localStorage.getItem('token');
  return request('/api/v1/hotfix/download_hotfix/', {
    method: 'GET',
    responseType: 'blob', // 必须加这属性，说明是文件流
    getResponse: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    params: {
      id
    },
    ...(options || {}),
  });
}

export async function postChangeOsType(params, options){
  const token = localStorage.getItem('token');
  return request('/api/v1/hotfix/update_ostype/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: {
      "id": params.id,
      "os_type": params.os_type,
      "source_repo": params.source_repo,
      "image": params.image,
      "source_devel": params.source_devel,
      "source_debuginfo": params.source_debuginfo,
    },
    ...(options || {}),
  })
}

export async function postRebuild(params, options){
  const token = localStorage.getItem('token');
  return request('/api/v1/hotfix/rebuild_hotfix/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: {
      "hotfix_id": params
    },
    ...(options || {}),
  })
}

//hotfix -> 自定义内核版本配置 -> 内核版本配置 表格编辑 接口请求
export async function postChangeKernelVersion(params, options){
  const token = localStorage.getItem('token');
  return request('/api/v1/hotfix/update_kernel_relation/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: {
      "id": params.id,
      "kernel_version": params.kernel_version,
      "os_type": params.os_type,
      "source": params.source,
      "devel_link": params.devel_link,
      "debuginfo_link": params.debuginfo_link,
      "image": params.image,
      "use_src_package": params.use_src_package,
    },
    // params: params,
    ...(options || {}),
  })
}

export async function getOSTypeList(params, options) {
  const token = localStorage.getItem('token');
  return request('/api/v1/hotfix/get_os_type_relation/', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': token,
  },
  params: params,
  ...(options || {}),
});
}

export async function getKernelVersionList(params, options) {
  const token = localStorage.getItem('token');
  return request('/api/v1/hotfix/get_kernel_relation/', {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': token,
  },
  params: params,
  ...(options || {}),
});
}

export async function submitOSType(params, options) {
  const token = localStorage.getItem('token');
  return request('/api/v1/hotfix/create_os_type_relation/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: {
      "os_type":params.os_type,
      "source_repo":params.source_repo,
      "src_pkg_mark": params.src_pkg_mark,
      "image": params.image,
      "source_devel": params.source_devel,
      "source_debuginfo": params.source_debuginfo,
      "sync_conf": params.sync_conf,
    },
    params: params,
    ...(options || {}),
  });
}

//hotfix -> 自定义内核版本配置 -> 内核版本配置 添加 接口请求
export async function submitKernelVersion(params, options) {
  const token = localStorage.getItem('token');
  return request('/api/v1/hotfix/create_kernel_relation/', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Authorization': token,
  },
  data:{
    "os_type": params.os_type,
    "kernel_version": params.kernel_version,
    "source": params.source,
    "devel_link": params.devel_link,
    "debuginfo_link": params.debuginfo_link,
    "image": params.image,
    "use_src_package": params.use_src_package,
  },
  // params: params,
  ...(options || {}),
});
}

export async function delOSType(id, token, options) {
  return request('/api/v1/hotfix/delete_os_type/', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: {
      id
    },
    ...(options || {}),
  })
}

// hotfix -> 自定义内核版本配置 -> 内核版本配置 表格删除 接口请求
export async function delKernelVersion(id, token, options) {
  return request('/api/v1/hotfix/delete_kernel_relation/', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: {
      id
    },
    ...(options || {}),
  })
}

// hotfix -> 自定义内核版本配置 -> 操作系统配置 表格同步 接口请求
export async function PostWhetherSyncRequest(params, options) {
  const token = localStorage.getItem('token');
  return request('/api/v1/hotfix/sync_kernel/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: {
      "id": params
    },
    // params: params,
    ...(options || {}),
  });
}

export async function postOneclickDeployment(params, options){
  const token = localStorage.getItem('token');
  return request('/api/v1/hotfix/oneclick_deploy/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: {
      "kernel_version": params.kernel_version,
      "rpm_package": params.rpm_package,
    },
    ...(options || {}),
  })
}

export async function getReleasedHotfixList(params, options){
    const token = localStorage.getItem('token')
    return request('/api/v1/hotfix/get_released_hotfixs/',{
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': token,
        },
        params: params,
    ...(options || {}),
    })
}

export async function updateChangeReleasedHotfixInfo(pk, data, options){
    const token = localStorage.getItem('token');
    return request(`/api/v1/hotfix/update_put_released_hotfix_info/${pk}/`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': token,
        },
        data: data,
        ...(options || {}),
      })
}

//hotfix -> 历史发布热补丁列表 -> 提交
export async function submitReleasedHotfixInfo(data, options) {
    const token = localStorage.getItem('token');
    return request('/api/v1/hotfix/insert_released_hotfix_info/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    data: data,
    // params: params,
    ...(options || {}),
  });
  }

export async function filterReleasedHotfixInfo(params, options){
    const token = localStorage.getItem('token');
    return request('/api/v1/hotfix/get_filtered_released_hotfix/', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': token,
    },
    params: params,
    ...(options || {}),
  });
}

/**
 * 批量导入hotfix released
 * @param {object} data
 * @param {object} options 
 * @returns 
 */
export async function batchAddHotfixReleased(data, options) {
  const token = localStorage.getItem('token');
  return request('/api/v1/hotfix/import_from_tablefiles/', {
    method: 'post',
    data: data,
    headers: {
      'Authorization': token
    },
    ...(options || {})
  })
}
