import { create } from "zustand";

export const useMockStore = create((set) => ({
    data: {
        datas: {},
    },
    setMockStore: (datas) => set({ data: datas }),
}))