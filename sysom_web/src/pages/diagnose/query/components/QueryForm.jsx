/*
 * @Author: wb-msm241621
 * @Date: 2024-03-05 18:04:46
 * @LastEditTime: 2024-03-11 16:08:58
 * @Description: 
 */
import { useRef } from 'react';
import ProCard from '@ant-design/pro-card';
import { useIntl, FormattedMessage } from 'umi';
import ProForm, {
    ProFormText,
    ProFormSelect,
    ProFormList,
    ProFormGroup
} from '@ant-design/pro-form';


export default (props) => {
    const int1 = useIntl();
    const resultFormRef = useRef();
    const { task_id, channel, params } = props?.params;
    return (
        <ProCard>
            <ProForm
                formRef={resultFormRef}
                onFinish={(v) => { props?.onDone?.(v) }}
                submitter={{
                    searchConfig: {
                        submitText: <FormattedMessage id="pages.hotfix.submit" defaultMessage="submit" />,
                        resetText: <FormattedMessage id="pages.diagnose.reset" defaultMessage="reset" />
                    },
                    onReset: () => {
                        resultFormRef.current.resetFields()
                        props?.onReset()
                    }
                }}
            >
                <ProFormGroup>
                    <ProFormText
                        name="task_id"
                        label={int1.formatMessage({
                            id:"pages.diagnose.diagnosisID",
                            defaultMessage: "Diagnosis ID"
                        })}
                        width="md"
                        initialValue={task_id}
                    />
                    <ProFormSelect
                        name="channel"
                        label={int1.formatMessage({
                            id: "pages.diagnose.diagnosisChannels",
                            defaultMessage: "Diagnosis Channels"
                        })}
                        width="md"
                        valueEnum={props.chanelEnum}
                        initialValue={channel}
                    />
                    <ProFormSelect
                        name="service_name"
                        width="md"
                        label={int1.formatMessage({
                            id: "pages.diagnose.diagnosisName",
                            defaultMessage: "Diagnosis Name"
                        })}
                        valueEnum={props.serviceNameEnum}
                    />
                </ProFormGroup>
                <ProFormList
                    name="params"
                    creatorButtonProps={{
                        creatorButtonText: <FormattedMessage id="pages.diagnose.generatePanel.newLine" defaultMessage="new Line" />,
                        icon: true,
                        type: "link",
                        style: { width: 'unset' },
                    }}
                    label={int1.formatMessage({
                        id: "pages.diagnose.diagnosisParams",
                        defaultMessage: "Diagnose Params"
                    })}
                    copyIconProps={false}
                    initialValue={params}
                    itemRender={({ listDom, action }) => {
                        return (
                            <div
                                style={{
                                    display: 'inline-flex',
                                    marginInlineEnd: 5,
                                }}
                            >
                                {listDom}
                                {action}
                            </div>
                        )
                    }
                    }
                >
                    <ProFormGroup labelLayout="inline" size="small">
                        <ProFormText width="sm" placeholder="key" name="key" />
                        <span>:</span>
                        <ProFormText width="sm" placeholder="value" name="value" />
                    </ProFormGroup>
                </ProFormList>
            </ProForm>
        </ProCard>
    )
}
