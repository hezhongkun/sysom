/// <reference types="cypress" />

describe("SysOM Nginx Observer Dashboard Test", () => {
    beforeEach(() => {
        cy.login();
    })

    it("Nginx Observer test", () => {
        // 1. 访问集群监控页面
        cy.visit("app_observable/Nginx");

        // 2. 等待页面加载完成
        cy.wait(2000);

        // 3. 检查异常告警pannel
        cy.getPannelContentByTitle("异常告警分布（次数）").then($el => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data")
            } else {
                cy.wrap($el).contains("请求抖动")
                cy.wrap($el).contains("请求4xx")
                cy.wrap($el).contains("请求5xx")
                cy.wrap($el).contains("错误日志")
            }
        });

        // 4. 检查请求数pannel
        cy.getPannelContentByTitle("请求数").then($el => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data")
            } else {
                cy.wrap($el).find("ul li").should("have.length.gte", 1);
                cy.wrap($el).find("ul li").eq(0).contains("requests");
            }
        });

        // 5. 检查http status分布pannel
        cy.getPannelContentByTitle("http status分布").then($el => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data")
            } else {
                cy.wrap($el).find("ul li").should("have.length.gte", 1);
                cy.wrap($el).find("ul li").eq(0).contains("status_1xx");
                cy.wrap($el).find("ul li").eq(1).contains("status_2xx");
                cy.wrap($el).find("ul li").eq(2).contains("status_3xx");
                cy.wrap($el).find("ul li").eq(3).contains("status_4xx");
                cy.wrap($el).find("ul li").eq(4).contains("status_5xx");
            }
        });
        

        // 6. 检查响应时延pannel
        cy.getPannelContentByTitle("响应时延").then($el => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data")
            } else {
                cy.wrap($el).find("ul li").should("have.length.gte", 1);
                cy.wrap($el).find("ul li").eq(0).contains("requestTime");
                cy.wrap($el).find("ul li").eq(1).contains("upstreamTime");
                cy.wrap($el).find("ul li").eq(2).contains("maxRequestTime");
                cy.wrap($el).find("ul li").eq(3).contains("maxUpstreamTime");
            }
        });
        
        // 7. 检查workers数量pannel
        cy.getPannelContentByTitle("workers数量").then($el => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data")
            } else {
                cy.wrap($el).find("ul li").should("have.length.gte", 1);
                cy.wrap($el).find("ul li").eq(0).contains("workersCount");
            }
        });

        // 8. 检查活跃的连接数pannel
        cy.getPannelContentByTitle("活跃的连接数").then($el => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data")
            } else {
                cy.wrap($el).find("ul li").should("have.length.gte", 1);
                cy.wrap($el).find("ul li").eq(0).contains("activeConnections");
            }
        });

        // 9. 检查nginx进程cpu利用率pannel
        cy.getPannelContentByTitle("nginx进程cpu利用率").then($el => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data")
            } else {
                cy.wrap($el).find("ul li").should("have.length.gte", 1);
                cy.wrap($el).find("ul li").eq(0).contains(/\d+/).then(($el) => {
                    const num = parseFloat($el.text());
                    expect(num).to.be.gte(0);
                });
                cy.wrap($el).find("ul li").eq(1).contains(/\d+/).then(($el) => {
                    const num = parseFloat($el.text());
                    expect(num).to.be.gte(0);
                });
            }
        });

        // 10. 检查nginx进程内存利用率pannel
        cy.getPannelContentByTitle("nginx进程内存利用率").then($el => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data")
            } else {
                cy.wrap($el).find("ul li").should("have.length.gte", 1);
                cy.wrap($el).find("ul li").eq(0).contains(/\d+/).then(($el) => {
                    const num = parseFloat($el.text());
                    expect(num).to.be.gte(0);
                });
                cy.wrap($el).find("ul li").eq(1).contains(/\d+/).then(($el) => {
                    const num = parseFloat($el.text());
                    expect(num).to.be.gte(0);
                });
            }
        });

        // 11. 检查nginx进程网络流量pannel
        cy.getPannelContentByTitle("nginx进程网络流量").then($el => {
            if ($el.text().includes("No data")) {
                cy.wrap($el).contains("No data")
            } else {
                cy.wrap($el).find("ul li").should("have.length.gte", 1);
                cy.wrap($el).find("ul li").eq(0).contains(/\d+/).then(($el) => {
                    const num = parseFloat($el.text());
                    expect(num).to.be.gte(0);
                });
                cy.wrap($el).find("ul li").eq(1).contains(/\d+/).then(($el) => {
                    const num = parseFloat($el.text());
                    expect(num).to.be.gte(0);
                });
            }
        });
    })
})