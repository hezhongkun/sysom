/// <reference types="cypress" />

describe("SysOM Cluster Manager Test", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke schedmoni diagnosis, and check result", () => {
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/cpu/schedmoni",

            // 诊断参数
            {
                "instance": Cypress.env("HOSTS")[0],
                "timeout": "1",
		        "threshold": "1"
            },

            // 诊断结果处理（在此处判断诊断的结果数据是否符合预期）
            (result) => {
                cy.get('.ant-pro-card-title > div').scrollIntoView()
                cy.get('.ant-pro-card-title > div').should('contain.text', '诊断结果')
                cy.diagnosisTaskResultHandler(result, () => {
                    cy.get(':nth-child(1) > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "Event overview");
                    cy.get(':nth-child(1) > .ant-pro-card > .ant-pro-card-body').then(($el) => {
                        if ($el.text().includes("no data")){
                            cy.wrap($el).contains("no data")
                        } else {
                            cy.get(':nth-child(1) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should("match", /emergency|warning|normal/);
                            cy.get(':nth-child(2) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should("match", /emergency|warning|normal/);
                            cy.get(':nth-child(3) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should("match", /emergency|warning|normal/);
                        }
                    })
                    
                    cy.get(':nth-child(2) > .ant-pro-card-header').should("contain.text", "Timeline Diagram");
                    cy.get(':nth-child(3) > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "Scheduling Jitter Details");
                })
            })
    })
})
