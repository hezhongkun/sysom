/// <reference types="cypress" />

describe("vmcore list page", () => {
    it("show vmcore list", () => {
        cy.login()
        cy.visit("/vmcore/match")
        /* ==== Generated with Cypress Studio ==== */
        cy.get('#similar_dmesg').clear();
        //let inputText = " [6231329.685410] Kernel panic - not syncing: sysrq triggered crash \n[6231329.685933] CPU: 3 PID: 514785 Comm: bash Kdump: loaded Tainted: G        W   E     5.10.134-14.an8.x86_64 #1 \n[6231329.686813] Hardware name: Alibaba Cloud Alibaba Cloud ECS, BIOS 449e491 04/01/2014 \n[6231329.687515] Call Trace: \n[6231329.687759]  dump_stack+0x57/0x6e\n [6231329.688070]  panic+0x10d/0x2e9 \n[6231329.688392]  sysrq_handle_crash+0x16/0x20\n [6231329.688759]  __handle_sysrq.cold.18+0x7a/0xe8\n [6231329.689159]  write_sysrq_trigger+0x2b/0x40 \n [6231329.689548]  proc_reg_write+0x3b/0x80 \n[6231329.689887]  vfs_write+0xb5/0x260 \n[6231329.690203]  ksys_write+0x49/0xc0 \n[6231329.690523]  do_syscall_64+0x33/0x40 \n[6231329.690853]  entry_SYSCALL_64_after_hwframe+0x61/0xc6 \n[6231329.691325] RIP: 0033:0x7f4d40d205a8 ";
        //cy.get("#similar_dmesg").invoke('val', inputText)
        //cy.get('#similar_dmesg').type( " [6231329.685410] Kernel panic - not syncing: sysrq triggered crash \n[6231329.685933] CPU: 3 PID: 514785 Comm: bash Kdump: loaded Tainted: G        W   E     5.10.134-14.an8.x86_64 #1 \n[6231329.686813] Hardware name: Alibaba Cloud Alibaba Cloud ECS, BIOS 449e491 04/01/2014 \n[6231329.687515] Call Trace: \n[6231329.687759]  dump_stack+0x57/0x6e\n [6231329.688070]  panic+0x10d/0x2e9 \n[6231329.688392]  sysrq_handle_crash+0x16/0x20\n [6231329.688759]  __handle_sysrq.cold.18+0x7a/0xe8\n [6231329.689159]  write_sysrq_trigger+0x2b/0x40 \n [6231329.689548]  proc_reg_write+0x3b/0x80 \n[6231329.689887]  vfs_write+0xb5/0x260 \n[6231329.690203]  ksys_write+0x49/0xc0 \n[6231329.690523]  do_syscall_64+0x33/0x40 \n[6231329.690853]  entry_SYSCALL_64_after_hwframe+0x61/0xc6 \n[6231329.691325] RIP: 0033:0x7f4d40d205a8 ");
        cy.get('#similar_dmesg').type("123")
        cy.get(':nth-child(2) > .ant-btn').click();
        cy.get('table > tbody').eq(0).children().should('have.length.of.at.least',1)
        /* ==== End Cypress Studio ==== */
    })
})
