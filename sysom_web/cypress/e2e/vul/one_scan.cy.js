/// <reference types="cypress" />

describe("SysOM Vul Manager Test", () => {

    
    it("login success", () => {

        cy.login()  
        
        cy.intercept("POST", "api/v1/vul/updatesa/")
            .as("updatesa")

        // 1. 访问安全中心列表
        cy.visit("security/list")

        //2.一键扫描
        cy.get("button").contains("一键扫描").click()
        
        //3.判断扫描结果
        cy.wait('@updatesa')
            .then((interception) => {
                cy.wrap({
                    statusCode: interception.response?.statusCode
                }).its("statusCode").should("eq", 200)
            })
    }


    )
})