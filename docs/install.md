# 安装指南

## 1. 编译部署
### 1.1 依赖
- nodejs >= 12.0.0
- python >= 3.6
- git
- tar
- wget

### 1.2 编译

- 依赖安装

  ```bash
  dnf module install nodejs:16 -y
  npm install -g yarn
  dnf install git tar wget -y
  ```

- 编译打包
  > 注意：打包出的 release 包的命名格式为 `sysomRelease-xxx.tar.gz` ，其中 `xxx` 为打包时的时间，因此每次打包时生成的压缩包名称都是不同的

  ```bash
  git clone https://gitee.com/anolis/sysom.git
  cd sysom
  bash package.sh
  ```

### 1.3 部署

- 解压 release 包

  > 注意下面压缩包的名称要替换成实际打包出的 release 包的名称

  ```bash
  tar -zxvf sysomRelease-20230129142347.tar.gz
  cd sysomRelease-20230129142347
  ```

  解压完成后，Release 包内的文件分布如下：

  ```bash
  .
  ├── clear.sh
  ├── deploy.sh
  ├── script
  ├── sysom_server
  ├── sysom_web
  └── tools
  ```

- 使用 `deploy.sh` 脚本进行部署

  - 首先使用 `ifconfig` 查看当前机器的IP => 比如：`172.22.3.238`

  - `deploy.sh` 部署脚本参数说明

    ```bash
    bash deploy.sh <部署目录> <内网IP> <外网IP>
    ```

    - arg1 : 部署目录
    - arg2 : 内网IP（主要是方便内网通讯，用户需要保证内网能通）
    - arg3 : 外网IP（浏览器可以访问到的IP地址)

  - 使用 `deploy.sh` 脚本进行一键部署

    如果没有公网IP，均使用内网IP即可，实际部署时可以替换成公网IP

    ```bash
    ./deploy.sh /usr/local/sysom 172.22.3.238 172.22.3.238
    ```
