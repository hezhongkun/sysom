# -*- coding: utf-8 -*- #
"""
Time                2023/6/1 16:13
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                filter.py
Description:
"""
from enum import Enum


class FilterType(Enum):
    Wildcard = 0
    Equal = 1


class Filter:
    def __init__(self, label_name: str, value: str, filter_type: FilterType):
        self.label_name = label_name
        self.value = value
        self.filter_type = filter_type


class WildcardFilter(Filter):
    def __init__(self, label_name: str, value: str):
        super().__init__(label_name, value, FilterType.Wildcard)


class EqualFilter(Filter):
    def __init__(self, label_name: str, value: str):
        super().__init__(label_name, value, FilterType.Equal)
