# -*- coding: utf-8 -*- #
"""
Time                2023/5/5 11:42
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                exceptions.py
Description:
"""


class MetricReaderException(Exception):
    """MetricReader base exception

    This class defines the base exception for CMG, and all exceptions thrown
    by CMG should inherit from this class.
    """


class MetricReaderProtoAlreadyExistsException(MetricReaderException):
    """Exceptions thrown for duplicate proto(submodule) registration

    This exception should be thrown if the proto(submodule) already exists while
    registering a submodule.
    """


class MetricReaderProtoNotExistsException(MetricReaderException):
    """Exceptions thrown for trying to use a non-existent proto(submodule)

    Exceptions that will be thrown when trying to use a non-existent proto
    (submodule).
    """


class MetricReaderNotValidMetricReaderUrlException(MetricReaderException):
    """Exception thrown when an invalid MetricReaderUrl format is parsed."""
