# -*- coding: utf-8 -*- #
"""
Time                2022/12/1 11:50
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                test_redis_multi_consumer.py
Description:
"""
import unittest
import uuid
from cec_base.consumer import Consumer
from cec_base.admin import dispatch_admin, Admin
from cec_base.event import Event
from cec_base.producer import dispatch_producer, Producer
from cec_redis.redis_consumer import RedisConsumer
from cec_redis.redis_admin import RedisAdmin
from cec_redis.redis_producer import RedisProducer
from cec_base.cec_client import MultiConsumer, CecConsumeTaskException, \
    CecAsyncConsumeTask

URL = "redis://localhost:6379"


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        """
        This initialization function is executed when the test program
        starts
        """
        Consumer.register('redis', RedisConsumer)
        Admin.register('redis', RedisAdmin)
        Producer.register('redis', RedisProducer)

    def setUp(self) -> None:
        """
        This initialization function is executed before the execution of each
        test case
        """
        self.admin = dispatch_admin(URL)
        self.redis_admin: RedisAdmin = self.admin
        self.producer = dispatch_producer(URL)

        # 1. Create a topic for testing
        self.topic_name = str(uuid.uuid4())
        self.topic_name_2 = str(uuid.uuid4())
        self.assertEqual(self.admin.create_topic(self.topic_name), True)
        self.assertEqual(self.admin.create_topic(self.topic_name_2), True)

        # 2. Produce some test data for consumption
        for i in range(10):
            self.producer.produce(self.topic_name, {
                "seq": i
            }, callback=lambda e, msg, idx=i: setattr(self, f"msg{idx}", msg))

        for i in range(10):
            self.producer.produce(self.topic_name_2, {
                "seq": i + 10
            }, callback=lambda e, msg, idx=i + 10: setattr(self, f"msg{idx}",
                                                           msg))

        # 3. Check that the messages produced are normal (normal if they have
        #    an automatically assigned event_id)
        for i in range(10):
            self.assertNotEqual(getattr(self, f"msg{i}"), "")

    def tearDown(self) -> None:
        """
        After each test case is executed, this function is executed to perform
        some cleanup operations
        """
        print("tearDown")
        self.assertEqual(self.admin.del_topic(self.topic_name), True)

        self.admin.disconnect()
        self.producer.disconnect()

        for i in range(10):
            setattr(self, f"msg{i}", "")

    def test_multi_consume(self):
        def deal_event(event: Event, task: CecAsyncConsumeTask):
            print(event.value, task.task_id)

        # 1. Create consumer group 1
        group1_id = str(uuid.uuid4())
        self.assertEqual(self.admin.create_consumer_group(group1_id), True)

        multi_consumer = MultiConsumer(URL, custom_callback=deal_event)
        multi_consumer.append_group_consume_task(
            self.topic_name, group1_id
        )
        multi_consumer.append_group_consume_task(
            self.topic_name_2, group1_id
        )
        multi_consumer.start()
        multi_consumer.join(100)

        # 2. Delete consumer group 2
        self.assertEqual(self.admin.del_consumer_group(group1_id), True)


if __name__ == '__main__':
    unittest.main()
