# -*- coding: utf-8 -*- #
"""
Time                2023/12/12 11:07
Author:             mingfeng (SunnyQjm), zhangque (Wardenjohn)
Email               mfeng@linux.alibaba.com, ydzhang@linux.alibaba.com
File                kafka_producer.py
Description:        The producer of Kafka
"""
import json
from typing import Callable, Union

from cec_base.producer import Producer
from cec_base.event import Event
from cec_base.exceptions import TopicNotExistsException, CecException
from cec_base.url import CecUrl
from clogger import logger
from confluent_kafka import Producer as ConfluentKafkaProducer
from confluent_kafka.cimpl import Message
from .common import StaticConst, ClientBase
class KafkaProducer(Producer):
    """ A Kafka Based Producer
    """

    _EVENT_KEY_KAFKA_PRODUCER_MESSAGE = "_EVENT_KEY_KAFKA_PRODUCER_MESSAGE"

    def __init__(self, url: CecUrl, auto_mk_topic: bool = False, **kwargs) -> None:
        self._current_url = ""
        if 'default_max_len' in kwargs:
            self.default_max_len = kwargs['default_max_len']
        else:
            self.default_max_len = 1000
        #self.auto_mk_topic = auto_mk_topic
        #url.params['allow.auto.create.topics'] = auto_mk_topic
        # 1. define ConfluentKafkaProducer type object of _kafka_producer_client
        # 2. use connect_by_cec_url to connect to kafka server
        self._kafka_producer_client: ConfluentKafkaProducer = None
        url.params = StaticConst.parse_kafka_connection_params(url.params)
        self.connect_by_cec_url(url)
        self._current_cec_url = url

        # create an dict, to save the reflaction from topic_name => TopicMeta 
        self._topic_metas = {

        }

    def produce(self, topic_name: str, message_value: dict, callback: Callable[[Exception, Event], None] = None,
                partition: int = -1,
                 **kwargs):
        """Produce method
        Generate one event and push it into the event center

        Args:
            topic_name (str): _description_
            message_value (dict): _description_
            callback (Callable[[Exception, Event], None], optional): _description_. Defaults to None.
            partition (int):
                1. if partition is passed, event will be sent to this partition (not recommand)
                2. if a positive partition number is passed, but this partition is not exsit, an exception is raised.
                3. if a negative partition number is passed, The message will be evenly delivered to all partitions 
                using the built-in strategy (recommended) 
        Examples:
            >>> producer = dispatch_producer(
            ..."kafka://localhost:6379?password=123456")
            >>> producer.produce("test_topic", {"value": "hhh"})
        """
        def deal_callback(err, msg: Message, value: dict, cb: Callable[[Exception, Event], None] = None):
            """deal callback

            Args:
                err (_type_): _description_
                msg (Message): _description_
                value (dict): _description_
                cb (Callable[[Exception, Event, None]], optional): _description_. Defaults to None.
            
            Description:
                
            """
            if msg.error() is not None:
                err = msg.error()
            if err is not None:
                cb(CecException(err), None)
            event = Event(value, f"{msg.partition()}-{msg.offset()}")
            event.put(KafkaProducer._EVENT_KEY_KAFKA_PRODUCER_MESSAGE, msg)
            if cb is not None:
                cb(None, event)

        topic_exist = False

        if topic_name not in self._topic_metas or self._topic_metas[topic_name].error is not None:
            self._topic_metas[topic_name] = self._kafka_producer_client.list_topics(topic_name).topics[topic_name]
            if self._topic_metas[topic_name].error is None:
                topic_exist = True
        else:
            topic_exist = True

        if not topic_exist:
            callback(TopicNotExistsException(
                f"{self} Topic ({topic_name}) not exists."), None
            )
            return 
        
        params = {
            **kwargs,
            'callback': lambda err, msg: deal_callback(
                err, msg, message_value, callback
            )
        }
        # 指定分区处理
        if partition >= 0:
            params['partition'] = partition

        self._kafka_producer_client.produce(
            topic_name, json.dumps(message_value),
            **params
        )

    def flush(self, timeout = -1):
        """flush all cache event to kafka server immediatly

        Args:
            timout (int, optional): _description_. Defaults to -1.

        Description:
            this function is used to flush all cache event to server.
        """
        if timeout <= 0:
            timeout = -1
        else:
            timeout = timeout / 1000
        self._kafka_producer_client.flush(timeout)

    def connect_by_cec_url(self, cec_url: CecUrl):
        """connect to kafka server with the given cec url

        Args:
            cec_url (CecUrl): An object generated by the configured cec url
        """
        self._kafka_producer_client = ConfluentKafkaProducer({
            'bootstrap.servers': cec_url.netloc,
            'request.timeout.ms': 600000,
            **cec_url.params
        })
        # CecUrl.__str__() is overwritten, return the url string
        self._current_url = cec_url.__str__()
        return self

    def connect(self, url: str):
        cec_url = CecUrl.parse(url)
        return self.connect_by_cec_url(cec_url=cec_url)
    
    def __del__(self):
        self.disconnect()

    def disconnect(self):
        if self._kafka_producer_client is None:
            return
        self._kafka_producer_client = None
        print("disconnect with kafka server")