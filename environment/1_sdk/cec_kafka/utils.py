# -*- coding: utf-8 -*- #
"""
Time                2023/12/12 17:13
Author:             mingfeng (SunnyQjm), zhangque (Wardenjohn)
Email               mfeng@linux.alibaba.com, ydzhang@linux.alibaba.com
File                utils.py
Description:        Utils of cec_kafka
"""
from cec_base.url import CecUrl
from clogger import logger

def raise_if_not_ignore(is_ignore_exception: bool, exception: Exception):
    """Raise or ignore for specific exception

    Args:
      is_ignore_exception: Is ignore exception while `exception` be raised.
      exception: The exception want to check
    """
    if is_ignore_exception:
        # If you choose to ignore the exception, the ignored exception is
        # logged in the log as an exception
        logger.exception(exception)
        return False
    raise exception