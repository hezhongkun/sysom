#!/bin/bash
start_app() {
    result=$(systemctl is-active mariadb.service)
    if [ "$result" != "active" ]; then
        systemctl start mariadb.service
    fi
}

start_app
